<html>
<link rel="stylesheet" type="text/css" href="theme.css">
<script src="engine.js"></script>
<title>Troubleshooting Assistant</title>
<body onLoad="init();">
  <div id="header">
    <h2 id="headertitle">Troubleshooting Assistant</h2>
  </div>
  <div id="breadcrumb">
  </div>
  <div id="container">
    Application loading...
  </div>
  <div id="footer">
    <div id="footer-left"><a onclick="renderAdmin()" href="#">Admin</a> | Edit mode: <a href="#" onClick="toggleEdit()" id="editToggle">on -> off</a></div><div id="footer-right"><big>&#9880;</big> Grown with &hearts; by Robert R. <big>&#9880;</big></div>
  </div>
</body>
</html>
