var contentData;
var xml;
var editSwitch = 0;
var editId = -1;
var newItemSwitch = 0;
var parent = 0;
var divStatus = [];
var breadcrumb = [];
var configXML;

function init()
{
  fetch("arg=init");
}

function makeFormating(input)
{
  function checkTag(tag)
  {
    var tagMatch = input.match(new RegExp(tag, "g"));
    if( tagMatch != undefined)
    {
      return tagMatch.length;
    }
    else
    {
      return 0;
    }
  }
  var uAmt = checkTag("&lt;u&gt;");
  var bAmt = checkTag("&lt;b&gt;");
  var iAmt = checkTag("&lt;i&gt;");
  var brAmt = checkTag("&lt;br&gt;");
  var centerAmt = checkTag("&lt;center&gt;");
  var rightAmt = checkTag("&lt;right&gt;");
  var leftAmt = checkTag("&lt;left&gt;");
  var httpAmt = checkTag("http://");
  var httpsAmt = checkTag("https://");
  var output = input;
  if(uAmt > 0)
  {
    var ucAmt = checkTag("&lt;/u&gt;");
    if(uAmt == ucAmt)
    {
      output = output.replace(/&lt;u&gt;/g,"<u>");
      output = output.replace(/&lt;\/u&gt;/g,"</u>");
    }
  }
  if(iAmt > 0)
  {
    var icAmt = checkTag("&lt;/i&gt;");
    if(iAmt == icAmt)
    {
      output = output.replace(/&lt;i&gt;/g,"<i>");
      output = output.replace(/&lt;\/i&gt;/g,"</i>");
    }
  }
  if(bAmt > 0)
  {
    var bcAmt = checkTag("&lt;/b&gt;");
    if(bAmt == bcAmt)
    {
      output = output.replace(/&lt;b&gt;/g,"<b>");
      output = output.replace(/&lt;\/b&gt;/g,"</b>");
    }
  }
  if(centerAmt > 0)
  {
    var centercAmt = checkTag("&lt;/center&gt;");
    if(centerAmt == centercAmt)
    {
      output = output.replace(/&lt;center&gt;/g,"<center>");
      output = output.replace(/&lt;\/center&gt;/g,"</center>");
    }
  }
  if(rightAmt > 0)
  {
    var rightcAmt = checkTag("&lt;/right&gt;");
    if(rightAmt == rightcAmt)
    {
      output = output.replace(/&lt;right&gt;/g,"<div width='100%' align='right'>");
      output = output.replace(/&lt;\/right&gt;/g,"</div>");
    }
  }
  if(leftAmt > 0)
  {
    var leftcAmt = checkTag("&lt;/left&gt;");
    if(leftAmt == leftcAmt)
    {
      output = output.replace(/&lt;left&gt;/g,"<div width='100%' align='left'>");
      output = output.replace(/&lt;\/left&gt;/g,"</div>");
    }
  }
  if(brAmt > 0)
  {
    output = output.replace(/&lt;br&gt;/g,"<br />");
  }
  output = output.split("\n").join(" </br>");
  if(httpAmt > 0)
  {
    var i;
    var len = output.length;
    for(i=0; i < len; i++)
    {
      if(output.slice(i, i + 7) == "http://")
      {
        var hlink;
        var url = output.slice(i, len).split(" ")[0];
        hlink = "<a class='contentlink' target='_blank' href='" + url + "'>";
        if(url.length > 40)
        {
          hlink += url.substring(0,20) + "..." + url.substring(url.length-10, url.length);
        }
        else
        {
          hlink += url;
        }
        hlink += "</a>";
        output = output.slice(0, i) + hlink + output.slice(i+url.length, output.length);
        len=output.length;
        i+=hlink.length;
      }
    }
  }
  if(httpsAmt > 0)
  {
    var i;
    var len = output.length;
    for(i=0; i < len; i++)
    {
      if(output.slice(i, i + 8) == "https://")
      {
        var hlink;
        var url = output.slice(i, len).split(" ")[0];
        hlink = "<a class='contentlink' target='_blank' href='" + url + "'>";
        if(url.length > 40)
        {
          hlink += url.substring(0,20) + "..." + url.substring(url.length-10, url.length);
        }
        else
        {
          hlink += url;
        }
        hlink += "</a>";
        output = output.slice(0, i) + hlink + output.slice(i+url.length, output.length);
        len=output.length;
        i+=hlink.length;
      }
    }
  }
  return output;
}

function checkLevel()
{
  var legalChars = "1234567890";
  var level = document.getElementById('newItemLevel').value;
  var output = "";
  var i;
  for(i=0; i<level.length; i++)
  {
    if(legalChars.indexOf(level.substr(i,1)) != -1)
    {
      output += level.substr(i,1);
    }
  }
  if(output == "") output = '0';
  document.getElementById('newItemLevel').value = output;
}

function toggleEdit()
{
  if(editSwitch == 0)
  {
    editSwitch = 1;
    document.getElementById('editToggle').innerHTML = "on <- off";
    //document.getElementById('newItemButton').style.display = "inline";
    fetch("arg=populate&parent=" + parent);
  }
  else
  {
    editSwitch = 0;
    document.getElementById('editToggle').innerHTML = "on -> off";
    //document.getElementById('newItemButton').style.display = "none";
    fetch("arg=populate&parent=" + parent);
  }
}

function newItem()
{
  //document.getElementById('newItemButton').style.display = 'none';
  newItemSwitch = 1;
  document.getElementById('edit').innerHTML = "Name: <input type='text' id='newItemName' /><br/>Description:<br /><textarea id='newItemDesc'></textarea><br /><input type='button' value='Add' onClick='submitNewItem()' /><input type='button' value='Cancel' onClick='document.getElementById(\"edit\").style.display=\"none\"; newItemSwitch=0;' />";
  document.getElementById('edit').style.display = 'block';
}

function submitNewItem()
{
  if(document.getElementById('newItemName').value == "")
  {
    alert("Name field may not be blank.");
    return 0;
  }
  newItemSwitch = 0;
  if(document.getElementById('newItemDesc').value == "")
  {
    document.getElementById('newItemDesc').value = " ";
  }
  //document.getElementById('edit').style.display = 'none';
  fetch("arg=newItem" + "&name=" + document.getElementById('newItemName').value + "&desc=" + document.getElementById('newItemDesc').value + "&level=" + document.getElementById('newItemLevel').value + "&parent=" + parent);
  //document.getElementById('edit').innerHTML = '';
}

function remove(id, i)
{
  var q =  "Are you sure you would like to delete ";
  var qn = xml.getElementsByTagName('entry')[i].getElementsByTagName('name')[0].innerHTML;
  if(qn.length > 23)
  {
    qn = qn.substring(0,20) + "...";
  }
  q = q + '"' + qn + '"?';
  if(confirm(q) == true)
  {
    fetch("arg=remove&id=" + id + "&parent=" + parent);
  }
}

function edit(id)
{
  editId = id;
  newItemSwitch = 0;
  renderPage();
}

function editSave()
{
  var entries = xml.getElementsByTagName('entry');
  if(document.getElementById('editItemName').value != "")
  {
    if(document.getElementById('editItemDesc').value == "") document.getElementById('editItemDesc').value = " ";
    fetch("arg=edit&id=" + entries[editId].getElementsByTagName('id')[0].childNodes[0].nodeValue + "&name=" + document.getElementById('editItemName').value + "&desc=" + document.getElementById('editItemDesc').value + "&level=" + document.getElementById('newItemLevel').value + "&parent=" + parent);
    editId = -1;
  }
  else
  {
    alert("Name cannot be blank.");
  }
}

function configSave()
{
  if (document.getElementById('title').value != "")
  {
    configXML.getElementsByTagName('mode')[0].childNodes[0].nodeValue = document.getElementById('mode').value;
    configXML.getElementsByTagName('title')[0].childNodes[0].nodeValue = document.getElementById('title').value;
    configUpdate();
    fetch("arg=setConfig&mode=" + document.getElementById('mode').value + "&title=" + document.getElementById('title').value);
  }
  else
  {
    alert("Page title may not be blank.");
  }
}

function configUpdate()
{
  document.getElementById('headertitle').innerHTML = configXML.getElementsByTagName('title')[0].childNodes[0].nodeValue;
}

function breadcrumbClick(id)
{
  if(id == -1)
  {
    breadcrumb = [];
    parent = 0;
    fetch("arg=populate&parent=0");
  }
  else
  {
    var i;
    for(i = 0; i < breadcrumb.length; i++)
    {
      if(typeof breadcrumb[i] != 'undefined')
      {
        if(i==id)
        {
          breadcrumb.splice(i+1,breadcrumb.length);
        }
      }
    }
    fetch("arg=populate&parent=" + breadcrumb[id][0]);
  }
}

function renderPage()
{
  var output = "";
  var i;
  var entries = xml.getElementsByTagName('entry');
  var container = document.getElementById('container');
  output += "<small>";
  if (editSwitch == 1)
  {
    output += "<div style='width: 50%; text-align: left; display: inline-block;'>";
  }
  output += "<a href='#' onClick='breadcrumbClick(-1)'> Home</a> > ";
  if(parent != 0)
  {
    for(i = 0; i<breadcrumb.length; i++)
    {
      if(typeof breadcrumb[i] != 'undefined')
      {
        output += "<a href='#' onClick='breadcrumbClick(" + i + ")'>" + breadcrumb[i][1] + "</a> > ";
      }
    }
  }
  if (editSwitch == 1)
  {
    output += "</div><div style='width: 50%; text-align: right; display: inline-block; white-space: pre;'><a href='#' onClick='newItemSwitch = 1; editId = -1; renderPage();'>+New Item </a></div>";
  }
  if(newItemSwitch == 1)
  {
    output += "<br />Name: <input type='text' id='newItemName' style='background-color: #000030;' /> Level: <input type='text' size='3' id='newItemLevel' value='0' onchange='checkLevel()' /><br/>Description:<br /><textarea id='newItemDesc' style='background-color: #000030; width: 100%;' rows='5' placeholder='Leave this field blank if you would like to create a new category. You may use the HTML tags <u></u>, <i></i>, <b></b>, <left/right/center></left/right/center>, and <br> for text formatting. Http and https urls will automatically be turned into hyperlinks.'></textarea><br /><input type='button' value='Add' onClick='submitNewItem()' /><input type='button' value='Cancel' onClick='newItemSwitch=0; renderPage();' />";
  }
  if(editId != -1)
  {
    output += "<br />Name: <input type='text' id='editItemName' style='background-color: #000030;' value='" + entries[editId].getElementsByTagName('name')[0].childNodes[0].nodeValue + "' /> Level: <input type='text' size='3' id='newItemLevel' value='0' onchange='checkLevel()' /><br/>Description:<br /><textarea id='editItemDesc' style='background-color: #000030; width: 100%;' rows='5'>" + entries[editId].getElementsByTagName('content')[0].childNodes[0].nodeValue + "</textarea><br /><input type='button' value='Save' onClick='editSave()' /><input type='button' value='Cancel' onClick='editId=-1; renderPage();' />";
  }
  output += "</small>";
  document.getElementById("breadcrumb").innerHTML = output;
  output = "";
  while (container.lastChild)
  {
    container.removeChild(container.lastChild)
  }
  if (entries.length == 0)
  {
    var nil = document.createTextNode("Nothing to see here!");
    container.appendChild(nil);
    return;
  }
  function renderItem(it)
  {
    var newDiv = document.createElement("div");
    newDiv.id = 'contentDiv'; newDiv.style.display = 'block';
    var newItem = document.createElement("a");
    newItem.href = "#"; newItem.addEventListener("click", function(){ processClick(it) });
    if (entries[it].getElementsByTagName('content')[0].childNodes[0].nodeValue != " ")
    {
      newItem.innerHTML = entries[it].getElementsByTagName('name')[0].childNodes[0].nodeValue + " - <small><i>View</i></small>";
    }
    else
    {
      newItem.innerHTML = '<b>' + entries[it].getElementsByTagName('name')[0].childNodes[0].nodeValue + '</b>' + ' - <small><i>Expand</i></small>';
    }
    newDiv.appendChild(newItem);
    if(editSwitch == true)
    {
      newDiv.appendChild(document.createTextNode(" | "));
      var newSmall = document.createElement("small"); var newSmall2 = document.createElement("small");
      var newAnchor = document.createElement("a"); var newAnchor2 = document.createElement("a");
      newSmall.style.fontStyle = "italic";
      newAnchor.appendChild(document.createTextNode("Edit"));
      newAnchor.addEventListener("click", function(){ edit(it) });
      newAnchor.href = "#"; newAnchor2.href = "#";
      newSmall.appendChild(newAnchor); newDiv.appendChild(newSmall);
      newDiv.appendChild(document.createTextNode(" | "));
      newAnchor2.appendChild(document.createTextNode("Remove"));
      newAnchor2.addEventListener("click", function(){ remove(entries[it].getElementsByTagName('id')[0].childNodes[0].nodeValue, it) });
      newSmall2.appendChild(newAnchor2); newDiv.appendChild(newSmall2);
      //| <small><i><a href='#' onClick='edit(" + it + ")'>Edit</a></i></small> |
      //<small><i><a href='#' onClick='remove(" + entries[it].getElementsByTagName('id')[0].childNodes[0].nodeValue + ", " + it + ")'>Remove</a></i></small>";
    }
    divStatus[it] = false;
    var contentContainer = document.createElement("div");
    contentContainer.id = 'contentContainer' + entries[it].getElementsByTagName('id')[0].childNodes[0].nodeValue;
    contentContainer.style.display = 'none';
    contentContainer.innerHTML += makeFormating(entries[it].getElementsByTagName('content')[0].childNodes[0].nodeValue);
    newDiv.appendChild(contentContainer);
    container.appendChild(newDiv);
  }
  for(i=0; i < entries.length; i++)
  {
    renderItem(i);
  }
  /*for (i = 0; i < entries.length; i++)
  {
    output += '<div id="content';
    if(bgSwitch == 0)
    {
      output += '1';
      bgSwitch = 1;
    }
    else
    {
      output += '2';
      bgSwitch = 0;
    }
    output += '">';
    output += '<a href="#" onClick="processClick(' + i + ')">';
    if (entries[i].getElementsByTagName('content')[0].childNodes[0].nodeValue != " ")
    {
      output += entries[i].getElementsByTagName('name')[0].childNodes[0].nodeValue + " - <small><i>View</i></small>";
    }
    else
    {
      output += '<b>' + entries[i].getElementsByTagName('name')[0].childNodes[0].nodeValue + '</b>' + ' - <small><i>Expand</i></small>';
    }
    if(editSwitch == true) output += " | <small><i><a href='#' onClick='edit(" + i + ")'>Edit</a></i></small> | <small><i><a href='#' onClick='remove(" + entries[i].getElementsByTagName('id')[0].childNodes[0].nodeValue + ", " + i + ")'>Remove</a></i></small>";
    divStatus[i] = false;
    output += '</a><div id="contentContainer' + entries[i].getElementsByTagName('id')[0].childNodes[0].nodeValue + '" style="display:none;">' +
    makeFormating(entries[i].getElementsByTagName('content')[0].childNodes[0].nodeValue) +
    '</div></div>';
    //output += ": " + entries[i].getElementsByTagName('content')[0].childNodes[0].nodeValue + "</div>";
  }*/
  //document.getElementById('container').innerHTML = output;
  //document.getElementById('content').innerHTML = '<textarea width="400" height="400">' + xml.getElementsByTagName('entry')[1].getElementsByTagName('name')[0].childNodes[0].nodeValue + '</textarea>';
}

function renderAdmin()
{
  var i;
  function modeCheck(cmode) { if(cmode == configXML.getElementsByTagName('mode')[0].childNodes[0].nodeValue) return " selected"; }
  var settingsL = [
    "Page title: <br /><small><i>The title to be displayed in the header at the top of the page.</i></small>",
    "Admin password: <br /><small><i>The master account password.</i></small>",
    "User management: <br /><small><i>Add, modify, and delete users.</i></small>",
    "Viewing mode: <br /><small><i>777 mode is read/write for anyone who has access.<br />Read only means that the database is not editable except by admin.<br />User access mode means that access is filtered based off of user permissions.</i></small>"
  ];
  var settingsR = [
    "<input type='text' id='title' value=\"" + configXML.getElementsByTagName('title')[0].childNodes[0].nodeValue + "\"/>",
    "<input type='button' value='Change Password' />",
    "<input type='button' value='User Management' />",
    "<select id='mode'><option value='777'" + modeCheck("777") + ">777 Mode</option><option value='readonly'" + modeCheck("readonly") + ">Read Only</option><option value='user'" + modeCheck("user") + ">User Access</user></select>"
  ];
  document.getElementById("breadcrumb").innerHTML = "<small><a href='#' onClick='breadcrumbClick(-1)'> Home</a> > Administration</small>";
  while(document.getElementById("container").firstChild) { document.getElementById("container").removeChild(document.getElementById("container").firstChild); }
  var tbl = document.createElement("table");
  tbl.style.width = "100%"; tbl.style.border = "0px"; tbl.style.borderCollapse = "collapse";
  for(i=0; i < settingsL.length; i++)
  {
    var newTr = document.createElement("tr");
    newTr.id = 'contentDiv';
    tbl.appendChild(newTr);
    var newTdL = document.createElement("td"); var newTdR = document.createElement("td");
    newTdL.style.width = "50%"; newTdR.style.width = "50%"; newTdL.style.textAlign = "right"; newTdR.style.textAlign = "left";
    newTdL.innerHTML = settingsL[i]; newTdR.innerHTML = settingsR[i];
    newTr.appendChild(newTdL); newTr.appendChild(newTdR);
  }
  document.getElementById('container').appendChild(tbl);
  var saveButton = document.createElement("input"); var cancelButton = document.createElement("input");
  saveButton.type = "button"; cancelButton.type = "button";
  saveButton.value = "Save"; cancelButton.value = "Cancel";
  saveButton.onclick = function() { configSave(); }; cancelButton.onclick = function() { breadcrumbClick(-1) };
  var cnt = document.createElement("center"); var sm = document.createElement("small");
  sm.appendChild(saveButton); sm.appendChild(cancelButton); cnt.appendChild(sm);
  document.getElementById("container").appendChild(cnt);
}

function processClick(id)
{
  var entries = xml.getElementsByTagName('entry');
  if(entries[id].getElementsByTagName('content')[0].childNodes[0].nodeValue != " ")
  {
      if(divStatus[id] == false)
      {
        document.getElementById('contentContainer' + entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue).style.borderBottom = '1px white dashed';
        document.getElementById('contentContainer' + entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue).style.backgroundColor = '#000030';
        document.getElementById('contentContainer' + entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue).style.display = 'block';
        divStatus[id] = true;
      }
      else
      {
        document.getElementById('contentContainer' + entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue).style.display = 'none';
        divStatus[id] = false;
      }
  }
  else
  {
    parent = entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue;
    breadcrumb[/*entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue*/breadcrumb.length] = [entries[id].getElementsByTagName('id')[0].childNodes[0].nodeValue,entries[id].getElementsByTagName('name')[0].childNodes[0].nodeValue,entries[id].getElementsByTagName('parent')[0].childNodes[0].nodeValue];
    fetch("arg=populate&parent=" + parent);
  }
}

function ajaxResponse(result)
{
  if(result.getElementsByTagName('results')[0].getAttribute('type') == 'populateContent')
  {
    xml=result;
    parent = xml.getElementsByTagName('results')[0].getAttribute('parent');
    renderPage();
  }
  if(result.getElementsByTagName('results')[0].getAttribute('type') == 'error')
  {
    alert(result.getElementsByTagName('error')[0].childNodes[0].nodeValue);
  }
  if(result.getElementsByTagName('results')[0].getAttribute('type') == 'config')
  {
    if(result.getElementsByTagName('apassword')[0].innerHTML == "")
    {
      alert("Admin password must be set before this application can be used.");
      configXML = result;
      configUpdate();
      renderAdmin();
    }
    else
    {
      configXML = result;
      configUpdate();
      fetch("arg=populate&parent=0");
    }
  }
  if(result.getElementsByTagName('results')[0].getAttribute('type') == "notification")
  {
    alert(result.getElementsByTagName('notification')[0].innerHTML);
  }
}

function fetch(arg)
{
  contentData = "loading...";
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function()
  {
    if(this.readyState == 4 && this.status == 200)
    {
      //document.getElementById('content').innerHTML = '<textarea width="400" height="400">' + this.responseText + '</textarea>';
      console.log(this.responseText);
      //xml=this.responseXML;
      var parser = new DOMParser();
      var xmlCheck = parser.parseFromString(this.responseText, "text/xml");
      if(xmlCheck.getElementsByTagName("parsererror") > 0)
      {
        //alert("Internal application error.");
        console.log("Invalid XML received:");
        console.log(this.responseText);
      }
      else
      {
        ajaxResponse(this.responseXML);
      }
    }
  };
  ajax.open("POST", "engine.php", true);
  ajax.overrideMimeType('application/xml');
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(arg);
}
