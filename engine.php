<?php
$db = new SQLite3("troubleshooter.db");
function dbSelect($query, $verify)
{
  global $db;
  $op = $db -> query($query);
  return $op;
}
function dbInsert($query, $verify)
{
  global $db;
  $op = $db -> query($query);
  return $op;
}
function sendError($err)
{
  $output = xmlwriter_open_memory();
  xmlwriter_set_indent($output, 1);
  xmlwriter_start_document($output, "1.0", "utf-8");
  xmlwriter_set_indent_string($output, "  ");
  xmlwriter_start_element($output, "results");
  xmlwriter_write_attribute($output, "type", "error");
  xmlwriter_start_element($output, "error");
  xmlwriter_text($output, $err);
  xmlwriter_end_element($output);
  xmlwriter_end_element($output);
  xmlwriter_end_document($output);
  return xmlwriter_output_memory($output);
}
function sendNotification($notification)
{
  $output = xmlwriter_open_memory();
  xmlwriter_set_indent($output, 1);
  xmlwriter_start_document($output, "1.0", "utf-8");
  xmlwriter_set_indent_string($output, "  ");
  xmlwriter_start_element($output, "results");
  xmlwriter_write_attribute($output, "type", "notification");
  xmlwriter_start_element($output, "notification");
  xmlwriter_text($output, $notification);
  xmlwriter_end_element($output);
  xmlwriter_end_element($output);
  xmlwriter_end_document($output);
  return xmlwriter_output_memory($output);
}
function sendConfig()
{
  global $db;
  $queryContent = $db -> query("SELECT * FROM 'config'");
  $output = xmlwriter_open_memory();
  xmlwriter_set_indent($output, 1);
  xmlwriter_set_indent_string($output, "  ");
  xmlwriter_start_document($output, "1.0", "utf-8");
  xmlwriter_start_element($output, "results");
  xmlwriter_write_attribute($output, "type", "config");
  while($row = $queryContent -> fetchArray())
  {
    xmlwriter_start_element($output, $row['setting']);
    xmlwriter_text($output, $row['value']);
    xmlwriter_end_element($output);
  }
  xmlwriter_end_element($output);
  xmlwriter_end_document($output);
  return xmlwriter_output_memory($output);
}
function populateDB()
{
  global $db;
  $error = "";
  $cdCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='contentData'");
  $cCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='config'");
  $uCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='users'");
  if($cdCheck -> fetchArray() === false)
  {
    $contentData = $db -> query("CREATE TABLE if NOT EXISTS 'contentData'('id' INTEGER PRIMARY KEY, 'name' TEXT, 'content' TEXT, 'parent' INTEGER, 'level' INTEGER)");
  }
  if($cCheck -> fetchArray() === false)
  {
    $config = $db -> query("CREATE TABLE if NOT EXISTS 'config'('setting' TEXT, 'value' TEXT)");
    $db -> query("INSERT INTO config(setting, value) VALUES ('apassword',''), ('mode', '777'), ('title', 'Troubleshooting Assistant')");
  }
  if($uCheck -> fetchArray() === false)
  {
    $users = $db -> query("CREATE TABLE if NOT EXISTS 'users'('uid' INTEGER PRIMARY KEY, 'username' TEXT, 'password' TEXT, 'level' INTEGER)");
  }
  $cdCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='contentData'");
  $cCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='config'");
  $uCheck = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='users'");
  if($cdCheck -> fetchArray() === false || $cCheck -> fetchArray() === false || $uCheck -> fetchArray() === false)
  {
    return sendError("Error creating database tables. Please check your server's PHP and SQLite configuration to ensure that table creation is possible.");
  }
  return sendConfig();
}
function populateContent($parent)
{
  global $db;
  $output = xmlwriter_open_memory();
  xmlwriter_set_indent($output, 1);
  xmlwriter_set_indent_string($output, "  ");
  xmlwriter_start_document($output, "1.0", "utf-8");
  xmlwriter_start_element($output, "results"); xmlwriter_write_attribute($output, "type", "populateContent"); xmlwriter_write_attribute($output, "parent", $parent);
  $queryContent = $db -> query("SELECT * FROM 'contentData' WHERE parent = " . $parent);
  while ($row = $queryContent -> fetchArray())
  {
    xmlwriter_start_element($output, "entry");
    xmlwriter_start_element($output, "id"); xmlwriter_text($output, $row['id']); xmlwriter_end_element($output);
    xmlwriter_start_element($output, "name"); xmlwriter_text($output, $row['name']); xmlwriter_end_element($output);
    xmlwriter_start_element($output, "content"); xmlwriter_text($output, $row['content']); xmlwriter_end_element($output);
    xmlwriter_start_element($output, "parent"); xmlwriter_text($output, $row['parent']); xmlwriter_end_element($output);
    xmlwriter_end_element($output);
  }
  xmlwriter_end_element($output);
  xmlwriter_end_document($output);
  return xmlwriter_output_memory($output);
}
if($_POST["arg"] == "newItem")
{
  dbInsert('INSERT INTO contentData("name", "content", "level", "parent") VALUES("' . htmlspecialchars($_POST['name']) . '", "' . htmlspecialchars($_POST['desc']) . '", ' . $_POST['level'] . ', ' . $_POST['parent'] . ')', false);
  echo populateContent($_POST['parent']);
}
if($_POST["arg"] == "populate")
{
  echo populateContent($_POST["parent"]);
}
if($_POST["arg"] == "remove")
{
  $search = dbSelect("SELECT * FROM contentData WHERE parent=" . $_POST['id'], 0);
  $count = $search -> fetchArray();
  if($count['id'] == null)
  {
    $db -> query("DELETE FROM contentData WHERE id=" . $_POST['id']);
    echo populateContent($_POST["parent"]);
  }
  else {
    echo sendError("Please delete all child entries before deleting a category.");
  }
}
if($_POST["arg"] == "edit")
{
  $search = dbSelect("SELECT * FROM contentData WHERE parent=" . $_POST['id'], 0);
  $count = $search -> fetchArray();
  if($count['id'] != null)
  {
    if($_POST['desc'] != " ")
    {
      exit(sendError("You cannot change a category into an item when it still has child items, this will make the child items unreachable.</error>"));
    }
  }
  dbInsert('UPDATE contentData SET name="' . htmlspecialchars($_POST['name']) . '", content="' . htmlspecialchars($_POST['desc']) . '", level="' . $_POST['level'] . '" WHERE id="' . $_POST['id'] . '"', false);
  echo populateContent($_POST['parent']);
}
if($_POST["arg"] == "setConfig")
{
  $db -> query('UPDATE config SET value="' . htmlspecialchars($_POST['title']) . '" WHERE setting="title"');
  $db -> query('UPDATE config SET value="' . htmlspecialchars($_POST['mode']) . '" WHERE setting="mode"');
  echo sendNotification("Settings have been changed successfully.");
}
if($_POST["arg"] == "init")
{
  if($db)
  {
    $contentdata = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='contentData'");
    $config = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='config'");
    $users = $db -> query("SELECT name FROM sqlite_master WHERE type='table' AND name='users'");
    if($contentdata -> fetchArray() === false || $config -> fetchArray() === false || $users -> fetchArray() === false)
    {
      echo populateDB();
    }
    else
    {
      //$setupCheck = $db -> query("SELECT value FROM config WHERE setting = 'apassword'");
      //if($setupCheck -> fetchArray()[0] == "")
      //{
      echo sendConfig();
      //}
      //else
      //{
      //echo populateContent(0);
      //}
    }
  }
  else
  {
    echo sendError("Error initializing database. Please check read/write permissions on troubleshooter directory.");
  }
}
?>
